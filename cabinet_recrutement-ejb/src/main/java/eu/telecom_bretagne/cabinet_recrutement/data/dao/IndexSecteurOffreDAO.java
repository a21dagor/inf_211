package eu.telecom_bretagne.cabinet_recrutement.data.dao;
// Generated Feb 15, 2023, 1:47:49 PM by Hibernate Tools 5.4.20.Final


import eu.telecom_bretagne.cabinet_recrutement.data.model.IndexSecteurOffre;

import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 * Home object for domain model class IndexSecteurOffre.
 * @see eu.telecom_bretagne.cabinet_recrutement.data.model.IndexSecteurOffre
 * @author Hibernate Tools
 */
@Stateless
public class IndexSecteurOffreDAO {

    private static final Logger logger = Logger.getLogger(IndexSecteurOffreDAO.class.getName());

    @PersistenceContext private EntityManager entityManager;
    
    public void persist(IndexSecteurOffre transientInstance) {
        logger.log(Level.INFO, "persisting IndexSecteurOffre instance");
        try {
            entityManager.persist(transientInstance);
            logger.log(Level.INFO, "persist successful");
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "persist failed", re);
            throw re;
        }
    }
    
    public void remove(IndexSecteurOffre persistentInstance) {
        logger.log(Level.INFO, "removing IndexSecteurOffre instance");
        try {
            entityManager.remove(persistentInstance);
            logger.log(Level.INFO, "remove successful");
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "remove failed", re);
            throw re;
        }
    }
    
    public IndexSecteurOffre merge(IndexSecteurOffre detachedInstance) {
        logger.log(Level.INFO, "merging IndexSecteurOffre instance");
        try {
            IndexSecteurOffre result = entityManager.merge(detachedInstance);
            logger.log(Level.INFO, "merge successful");
            return result;
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "merge failed", re);
            throw re;
        }
    }
    
    public IndexSecteurOffre findById( int id) {
        logger.log(Level.INFO, "getting IndexSecteurOffre instance with id: " + id);
        try {
            IndexSecteurOffre instance = entityManager.find(IndexSecteurOffre.class, id);
            logger.log(Level.INFO, "get successful");
            return instance;
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "get failed", re);
            throw re;
        }
    }
}

