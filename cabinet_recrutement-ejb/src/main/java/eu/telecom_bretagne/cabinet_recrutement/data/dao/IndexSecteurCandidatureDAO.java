package eu.telecom_bretagne.cabinet_recrutement.data.dao;
// Generated Feb 15, 2023, 1:47:49 PM by Hibernate Tools 5.4.20.Final


import eu.telecom_bretagne.cabinet_recrutement.data.model.IndexSecteurCandidature;

import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 * Home object for domain model class IndexSecteurCandidature.
 * @see eu.telecom_bretagne.cabinet_recrutement.data.model.IndexSecteurCandidature
 * @author Hibernate Tools
 */
@Stateless
public class IndexSecteurCandidatureDAO {

    private static final Logger logger = Logger.getLogger(IndexSecteurCandidatureDAO.class.getName());

    @PersistenceContext private EntityManager entityManager;
    
    public void persist(IndexSecteurCandidature transientInstance) {
        logger.log(Level.INFO, "persisting IndexSecteurCandidature instance");
        try {
            entityManager.persist(transientInstance);
            logger.log(Level.INFO, "persist successful");
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "persist failed", re);
            throw re;
        }
    }
    
    public void remove(IndexSecteurCandidature persistentInstance) {
        logger.log(Level.INFO, "removing IndexSecteurCandidature instance");
        try {
            entityManager.remove(persistentInstance);
            logger.log(Level.INFO, "remove successful");
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "remove failed", re);
            throw re;
        }
    }
    
    public IndexSecteurCandidature merge(IndexSecteurCandidature detachedInstance) {
        logger.log(Level.INFO, "merging IndexSecteurCandidature instance");
        try {
            IndexSecteurCandidature result = entityManager.merge(detachedInstance);
            logger.log(Level.INFO, "merge successful");
            return result;
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "merge failed", re);
            throw re;
        }
    }
    
    public IndexSecteurCandidature findById( int id) {
        logger.log(Level.INFO, "getting IndexSecteurCandidature instance with id: " + id);
        try {
            IndexSecteurCandidature instance = entityManager.find(IndexSecteurCandidature.class, id);
            logger.log(Level.INFO, "get successful");
            return instance;
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "get failed", re);
            throw re;
        }
    }
}

