package eu.telecom_bretagne.cabinet_recrutement.data.dao;
// Generated Feb 15, 2023, 1:47:49 PM by Hibernate Tools 5.4.20.Final


import eu.telecom_bretagne.cabinet_recrutement.data.model.MessagesCandidature;

import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 * Home object for domain model class MessagesCandidature.
 * @see eu.telecom_bretagne.cabinet_recrutement.data.model.MessagesCandidature
 * @author Hibernate Tools
 */
@Stateless
public class MessagesCandidatureDAO {

    private static final Logger logger = Logger.getLogger(MessagesCandidatureDAO.class.getName());

    @PersistenceContext private EntityManager entityManager;
    
    public void persist(MessagesCandidature transientInstance) {
        logger.log(Level.INFO, "persisting MessagesCandidature instance");
        try {
            entityManager.persist(transientInstance);
            logger.log(Level.INFO, "persist successful");
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "persist failed", re);
            throw re;
        }
    }
    
    public void remove(MessagesCandidature persistentInstance) {
        logger.log(Level.INFO, "removing MessagesCandidature instance");
        try {
            entityManager.remove(persistentInstance);
            logger.log(Level.INFO, "remove successful");
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "remove failed", re);
            throw re;
        }
    }
    
    public MessagesCandidature merge(MessagesCandidature detachedInstance) {
        logger.log(Level.INFO, "merging MessagesCandidature instance");
        try {
            MessagesCandidature result = entityManager.merge(detachedInstance);
            logger.log(Level.INFO, "merge successful");
            return result;
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "merge failed", re);
            throw re;
        }
    }
    
    public MessagesCandidature findById( int id) {
        logger.log(Level.INFO, "getting MessagesCandidature instance with id: " + id);
        try {
            MessagesCandidature instance = entityManager.find(MessagesCandidature.class, id);
            logger.log(Level.INFO, "get successful");
            return instance;
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "get failed", re);
            throw re;
        }
    }
}

