package eu.telecom_bretagne.cabinet_recrutement.data.dao;
// Generated Feb 15, 2023, 1:47:49 PM by Hibernate Tools 5.4.20.Final


import eu.telecom_bretagne.cabinet_recrutement.data.model.MessagesOffreEmploi;

import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 * Home object for domain model class MessagesOffreEmploi.
 * @see eu.telecom_bretagne.cabinet_recrutement.data.model.MessagesOffreEmploi
 * @author Hibernate Tools
 */
@Stateless
public class MessagesOffreEmploiDAO {

    private static final Logger logger = Logger.getLogger(MessagesOffreEmploiDAO.class.getName());

    @PersistenceContext private EntityManager entityManager;
    
    public void persist(MessagesOffreEmploi transientInstance) {
        logger.log(Level.INFO, "persisting MessagesOffreEmploi instance");
        try {
            entityManager.persist(transientInstance);
            logger.log(Level.INFO, "persist successful");
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "persist failed", re);
            throw re;
        }
    }
    
    public void remove(MessagesOffreEmploi persistentInstance) {
        logger.log(Level.INFO, "removing MessagesOffreEmploi instance");
        try {
            entityManager.remove(persistentInstance);
            logger.log(Level.INFO, "remove successful");
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "remove failed", re);
            throw re;
        }
    }
    
    public MessagesOffreEmploi merge(MessagesOffreEmploi detachedInstance) {
        logger.log(Level.INFO, "merging MessagesOffreEmploi instance");
        try {
            MessagesOffreEmploi result = entityManager.merge(detachedInstance);
            logger.log(Level.INFO, "merge successful");
            return result;
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "merge failed", re);
            throw re;
        }
    }
    
    public MessagesOffreEmploi findById( int id) {
        logger.log(Level.INFO, "getting MessagesOffreEmploi instance with id: " + id);
        try {
            MessagesOffreEmploi instance = entityManager.find(MessagesOffreEmploi.class, id);
            logger.log(Level.INFO, "get successful");
            return instance;
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "get failed", re);
            throw re;
        }
    }
}

