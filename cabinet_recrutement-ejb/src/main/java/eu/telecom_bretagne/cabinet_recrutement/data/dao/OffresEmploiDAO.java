package eu.telecom_bretagne.cabinet_recrutement.data.dao;
// Generated Feb 15, 2023, 1:47:49 PM by Hibernate Tools 5.4.20.Final


import eu.telecom_bretagne.cabinet_recrutement.data.model.OffresEmploi;

import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 * Home object for domain model class OffresEmploi.
 * @see eu.telecom_bretagne.cabinet_recrutement.data.model.OffresEmploi
 * @author Hibernate Tools
 */
@Stateless
public class OffresEmploiDAO {

    private static final Logger logger = Logger.getLogger(OffresEmploiDAO.class.getName());

    @PersistenceContext private EntityManager entityManager;
    
    public void persist(OffresEmploi transientInstance) {
        logger.log(Level.INFO, "persisting OffresEmploi instance");
        try {
            entityManager.persist(transientInstance);
            logger.log(Level.INFO, "persist successful");
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "persist failed", re);
            throw re;
        }
    }
    
    public void remove(OffresEmploi persistentInstance) {
        logger.log(Level.INFO, "removing OffresEmploi instance");
        try {
            entityManager.remove(persistentInstance);
            logger.log(Level.INFO, "remove successful");
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "remove failed", re);
            throw re;
        }
    }
    
    public OffresEmploi merge(OffresEmploi detachedInstance) {
        logger.log(Level.INFO, "merging OffresEmploi instance");
        try {
            OffresEmploi result = entityManager.merge(detachedInstance);
            logger.log(Level.INFO, "merge successful");
            return result;
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "merge failed", re);
            throw re;
        }
    }
    
    public OffresEmploi findById( int id) {
        logger.log(Level.INFO, "getting OffresEmploi instance with id: " + id);
        try {
            OffresEmploi instance = entityManager.find(OffresEmploi.class, id);
            logger.log(Level.INFO, "get successful");
            return instance;
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "get failed", re);
            throw re;
        }
    }
}

