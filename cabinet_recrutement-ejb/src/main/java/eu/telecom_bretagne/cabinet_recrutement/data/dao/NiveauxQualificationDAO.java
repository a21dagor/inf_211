package eu.telecom_bretagne.cabinet_recrutement.data.dao;
// Generated Feb 15, 2023, 1:47:49 PM by Hibernate Tools 5.4.20.Final


import eu.telecom_bretagne.cabinet_recrutement.data.model.NiveauxQualification;

import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 * Home object for domain model class NiveauxQualification.
 * @see eu.telecom_bretagne.cabinet_recrutement.data.model.NiveauxQualification
 * @author Hibernate Tools
 */
@Stateless
public class NiveauxQualificationDAO {

    private static final Logger logger = Logger.getLogger(NiveauxQualificationDAO.class.getName());

    @PersistenceContext private EntityManager entityManager;
    
    public void persist(NiveauxQualification transientInstance) {
        logger.log(Level.INFO, "persisting NiveauxQualification instance");
        try {
            entityManager.persist(transientInstance);
            logger.log(Level.INFO, "persist successful");
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "persist failed", re);
            throw re;
        }
    }
    
    public void remove(NiveauxQualification persistentInstance) {
        logger.log(Level.INFO, "removing NiveauxQualification instance");
        try {
            entityManager.remove(persistentInstance);
            logger.log(Level.INFO, "remove successful");
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "remove failed", re);
            throw re;
        }
    }
    
    public NiveauxQualification merge(NiveauxQualification detachedInstance) {
        logger.log(Level.INFO, "merging NiveauxQualification instance");
        try {
            NiveauxQualification result = entityManager.merge(detachedInstance);
            logger.log(Level.INFO, "merge successful");
            return result;
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "merge failed", re);
            throw re;
        }
    }
    
    public NiveauxQualification findById( int id) {
        logger.log(Level.INFO, "getting NiveauxQualification instance with id: " + id);
        try {
            NiveauxQualification instance = entityManager.find(NiveauxQualification.class, id);
            logger.log(Level.INFO, "get successful");
            return instance;
        }
        catch (RuntimeException re) {
            logger.log(Level.SEVERE, "get failed", re);
            throw re;
        }
    }
}

